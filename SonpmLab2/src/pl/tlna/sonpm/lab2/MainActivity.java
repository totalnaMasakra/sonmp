package pl.tlna.sonpm.lab2;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.sql.Time;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.TreeMap;

import org.xmlpull.v1.XmlPullParser;

import android.app.Dialog;
import android.app.ListActivity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.PowerManager;
import android.util.Log;
import android.util.Xml;
import android.view.View;
import android.widget.SimpleAdapter;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends ListActivity {

	// lista z danymi dla ListView
	private ArrayList<Map<String, String>> currencyList = new ArrayList<Map<String, String>>();
	
	// lista z danymi z parsowanych plików
	private TreeMap<String, HashMap<String, String>> parsedData = new TreeMap<String, HashMap<String, String>>();

	private SimpleAdapter listAdapter;	// adapter będący uchwytem do widoku
										// listy
	private ProgressDialog pDialog;		// pastek postępu podczas uruchamiania

	// adres do strony nbp z plikami xml
	private static final String urlPrefix = "http://www.nbp.pl/kursy/xml/";
	private static final String lastA = "LastA.xml";
	private static final String lastC = "LastC.xml";

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

		// list adapter będący spoiwem pomiędzy ListView a currencyList
		listAdapter = new SimpleAdapter(this, currencyList,
				R.layout.list_element, new String[] { "kod", "nazwa", "opis" },
				new int[] { R.id.kod, R.id.nazwa, R.id.opis });
		setListAdapter(listAdapter);

		// konfiguracja dialogu aktualizacji
		pDialog = new ProgressDialog(this);
		pDialog.setMessage("Pobieranie kursów NBP");

		// uruchamianie aktualizacji
		UpdateViewTask task = new UpdateViewTask(this);
		task.execute();

	}

/*	@Override
	public boolean onCreateOptionsMenu(Menu menu) {

		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {

			// HashMap<String,String> m = new HashMap<String,String>(); //
			// przykład dodawania do listy
			// m.put(TAG_HEAD, "nie");
			// m.put(TAG_FOOT, "Dokładnie nie");
			// currencyList.add( m );
			// listAdapter.notifyDataSetChanged();

			return true;
		}
		return super.onOptionsItemSelected(item);
	}*/

	public void onClick(View v) {
		String key = ((TextView)v.findViewById(R.id.kod)).getText().toString() ;
		
		HashMap<String, String> hm = parsedData.get(key);
		
		// custom dialog
		final Dialog dialog = new Dialog( this );
		dialog.setContentView(R.layout.on_click_dialog);
		dialog.setTitle( hm.get("nazwa_waluty").toString() );

		// set the custom dialog components
		TextView tv = (TextView) dialog.findViewById(R.id.przelicznik);
		tv.setText( hm.get("przelicznik") + " " + key + " = " );
		
		tv = (TextView) dialog.findViewById(R.id.sredni);
		tv.setText( hm.get("kurs_sredni") + " PLN" );
		
		tv = (TextView) dialog.findViewById(R.id.sprzedaz);
		if( hm.get("kurs_sprzedazy") == null ) tv.setText("-");
		else tv.setText( hm.get("kurs_sprzedazy") + " PLN" );
		
		tv = (TextView) dialog.findViewById(R.id.kupno);
		if( hm.get("kurs_kupna") == null ) tv.setText("-");
		else tv.setText( hm.get("kurs_kupna") + " PLN" );


		dialog.show();
	  }
	
	/***********************************************************************/
	private class UpdateViewTask extends AsyncTask<String, Integer, String> {

		private Context context;
		private PowerManager.WakeLock mWakeLock;
		private String publishDate;

		public UpdateViewTask(Context context) {
			this.context = context;
		}

		private String download(String dwnl, Time ut ) {
			InputStream input = null;
			OutputStream output = null;
			HttpURLConnection connection = null;
			URL url;
			
			// sprawdzanie, czy plik nie wymaga aktualizcji
			Calendar utCal = Calendar.getInstance();
			utCal.setTime(ut);
			Calendar last = Calendar.getInstance();
			last.setTimeInMillis( getPreferences(0).getLong( dwnl+"_update" , 0 ) );
			last.add( Calendar.HOUR_OF_DAY , - utCal.get(Calendar.HOUR_OF_DAY) );
			last.add( Calendar.MINUTE , - utCal.get(Calendar.MINUTE) );
			Calendar now = Calendar.getInstance();
			now.add( Calendar.HOUR_OF_DAY , - utCal.get(Calendar.HOUR_OF_DAY) );
			now.add( Calendar.MINUTE , - utCal.get(Calendar.MINUTE) );
			if(		last.get(Calendar.YEAR)			>=	now.get(Calendar.YEAR) &&
					last.get(Calendar.DAY_OF_YEAR)	>=	now.get(Calendar.DAY_OF_YEAR)
				) return "Baza aktualna";
			
			try {
				
				// łączenie się z serwerem
				url = new URL(urlPrefix + dwnl);
				connection = (HttpURLConnection) url.openConnection();
				connection.connect();

				// expect HTTP 200 OK, so we don't mistakenly save error report
				// instead of the file
				if (connection.getResponseCode() != HttpURLConnection.HTTP_OK) {
					return "Odpowiedź z serwera: "
							+ connection.getResponseCode() + " "
							+ connection.getResponseMessage();
				}

				// zapisywanie daty pobrania pliku do preferencji aplikacji
				SharedPreferences.Editor editor = getPreferences(0).edit();
				editor.putLong( dwnl+"_update" , (new Date()).getTime() );
				editor.commit();
				
				// download the file
				input = connection.getInputStream();
				output = openFileOutput(dwnl, Context.MODE_PRIVATE);
				
				// pobieranie i zapisywanie do pliku
				byte data[] = new byte[4096];
				int count;
				while ((count = input.read(data)) != -1) {
					// allow canceling with back button
					if (isCancelled()) {
						input.close();
						return null;
					}
					output.write(data, 0, count);
				}
				
			} catch (Exception e) {
				// jeśli wystąpił błąd przy pobieraniu, zeruje zmienną
				// i tym samym wymusza ponowne pobranie pliku
				SharedPreferences.Editor editor = getPreferences(0).edit();
				editor.putLong( dwnl+"_update" , 0 );
				editor.commit();
				return "Błąd: " + e.toString();
			} finally {
				try {
					if (output != null)
						output.close();
					if (input != null)
						input.close();
				} catch (IOException ignored) {
				}

				if (connection != null)
					connection.disconnect();
				
			}
			return null;
		}

		private void parse() {
			HashMap<String, String> m;
			NbpXmlParser nxpA = new NbpXmlParser(lastA);
			publishDate = nxpA.getPublishDate();

			while (true) {
				// parsowanie i sprawdzanie warunku pętli
				nxpA.nextPozycja();
				if( nxpA.isEmpty() ) break;
				// dodawanie do parsedData
				m = new HashMap<String, String>();
				m.put( "nazwa_waluty", nxpA.getNazwa() );
				m.put( "przelicznik", nxpA.getPrzelicznik() );
				m.put( "kurs_sredni", nxpA.getSredni() );
				parsedData.put( nxpA.getKod() , m );
			}
			
			NbpXmlParser nxpC = new NbpXmlParser(lastC);
			while (true) {
				// parsowanie i sprawdzanie warunku pętli
				nxpC.nextPozycja();
				if( nxpC.isEmpty() ) break;
				// dodawanie do parsedData
				parsedData.get( nxpC.getKod() ).put( "kurs_kupna" , nxpC.getKupna() );
				parsedData.get( nxpC.getKod() ).put( "kurs_sprzedazy" , nxpC.getSprzedazy() );
			}

		}
		
		private void updateView(){
			 HashMap<String, String> m;
			 Map.Entry<String, HashMap<String, String>> en;

			 // pobiera dane z parsedData i dodaje do uchwytu do listView
			 Iterator<Entry<String, HashMap<String, String>>> i = parsedData.entrySet().iterator();
			 while( i.hasNext() ) {
				 en = (Map.Entry<String, HashMap<String, String>>)i.next();
				 m = new HashMap<String, String>();
				 m.put("kod", en.getKey() );
				 m.put("nazwa", " - " + en.getValue().get("nazwa_waluty") );
				 m.put("opis", en.getValue().get("przelicznik") + " " + en.getKey() + " = " + en.getValue().get("kurs_sredni") + " PLN" );
				 currencyList.add(m);
			 }
		}

		@Override
		protected String doInBackground(String... params) {

			String dStateA, dStateC, ret = null;

			// sprawdzanie stanu połączenia
			ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
			NetworkInfo ni = cm.getActiveNetworkInfo();
			if (ni == null || ni.getState() == NetworkInfo.State.DISCONNECTED)
				ret = "Brak połączenia z internetem, korzystanie z zapisanej bazy";

			try {
				// jeśli ret==null, jest połączenie i pobiera pliki
				if (ret == null) {
					dStateA = download(lastA, Time.valueOf("12:15:00")); // aktualne kursy średnie
					if (dStateA != null)
						ret = lastA + ": " + dStateA;

					dStateC = download(lastC, Time.valueOf("08:15:00")); // aktualne kursy kupna i
												// sprzedaży
					if (dStateC != null)
						if (ret != null)
							ret += "\n" + lastC + ": " + dStateC;
						else
							ret = lastC + ": " + dStateC;
				}

				parse();
				
			} catch (NullPointerException e) {
				ret = "Brak ściągninętych informacji o kursach";
			} catch (Exception e) {
				Log.w( "tlna", e.getClass().toString() );
				ret = ("Coś poszło nie tak");
			}

//			Log.i("tlna", parsedData.toString() );
			return ret;
		}

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			// take CPU lock to prevent CPU from going off if the user
			// presses the power button during download
			PowerManager pm = (PowerManager) context
					.getSystemService(Context.POWER_SERVICE);
			mWakeLock = pm.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK,
					getClass().getName());
			mWakeLock.acquire();
			pDialog.show();
		}

		@Override
		protected void onPostExecute(String result) {
			listAdapter.notifyDataSetChanged();
			TextView dl = (TextView)findViewById(R.id.dateLabel);
			dl.setText( publishDate );
			mWakeLock.release();
			pDialog.dismiss();
			
			if( result!=null && ( result.equals("Brak ściągninętych informacji o kursach") || result.equals("Coś poszło nie tak") ) ){
				finish();
			}
			
			updateView();
			
			if (result != null)
				Toast.makeText(context, result, Toast.LENGTH_LONG).show();
//			else
//				Toast.makeText(context, "Zaktualizowano", Toast.LENGTH_SHORT).show();
		}
	}
	
	/**************************/
	private class NbpXmlParser {

		// uchwyty do pliku i parsera
		private InputStream input = null;
		private XmlPullParser parser = null;
		
		// data publikacji
		private String publishDate = null;
		public String getPublishDate(){ return publishDate; }
		
		// zmienne w tagach pozycja dla kategorii A
		private String nazwa_waluty	=null;				public String getNazwa(){ return nazwa_waluty; }
		private String przelicznik	=null;				public String getPrzelicznik(){ return przelicznik; }
		private String kod_waluty	=null;				public String getKod(){ return kod_waluty; }
		private String kurs_sredni	=null;				public String getSredni(){ return kurs_sredni; }
		// dodatkowe zmienne dla kategorii C
		private String kurs_kupna	=null;				public String getKupna(){ return kurs_kupna; }
		private String kurs_sprzedazy=null;				public String getSprzedazy(){ return kurs_sprzedazy; }
		// czy znaleziony kolejny tag
		private boolean isEmpty		=true;				public boolean isEmpty(){ return isEmpty; }
		
		
		public NbpXmlParser(String filename) {

			try {
				input = openFileInput(filename);

				parser = Xml.newPullParser();
				parser.setInput(input, null);

				String lastTag = null;
				int et = parser.getEventType();
				while ( et != XmlPullParser.END_DOCUMENT ) {

					if ( et == XmlPullParser.START_TAG) {
						if( parser.getName().equals("pozycja") ) break;
						lastTag = parser.getName();
//						Log.i("tlna", lastTag );
					}
					else if ( et == XmlPullParser.TEXT && !parser.isWhitespace() ) {
//						Log.i("tlna", parser.getText() );
						if( lastTag.equals("data_publikacji") ) publishDate = parser.getText();
					}

					et = parser.next();
				}

			} catch (Exception e) {
				Log.w("tlna", e.getMessage());
			} finally {
				try {
					input.close();
				} catch (Exception ignored) {}
			}
		}
		
		public void nextPozycja(){
			try {
				
				nazwa_waluty	= null;
				przelicznik		= null;
				kod_waluty		= null;
				kurs_sredni		= null;
				kurs_kupna		= null;
				kurs_sprzedazy	= null;
				

				// sprawdza, czy jest dostępny do czytania kolejny tag 'pozycja'
				isEmpty = true;
				parser.require(XmlPullParser.START_TAG, null, "pozycja");
				isEmpty = false;

				// w pętli sprawdza cały kolejny tag
				String lastTag = null;
				int et = parser.getEventType();
				while (!(	et == XmlPullParser.END_DOCUMENT ||
							( et==XmlPullParser.END_TAG && parser.getName().equals("pozycja") ) )) {

					if ( et == XmlPullParser.START_TAG) {
						lastTag = parser.getName();
//						Log.i("tlna", lastTag );
					}
					else if ( et == XmlPullParser.TEXT && !parser.isWhitespace() ) {
//						Log.i("tlna", parser.getText() );
						// na podstawie ostatniego tagu pobiera tekst i zapisuje do zmiennej
						if		( lastTag.equals("nazwa_waluty") )	nazwa_waluty = parser.getText();
						else if	( lastTag.equals("przelicznik") )	przelicznik = parser.getText();
						else if	( lastTag.equals("kod_waluty") )	kod_waluty = parser.getText();
						else if	( lastTag.equals("kurs_sredni") )	kurs_sredni = parser.getText();
						else if	( lastTag.equals("kurs_kupna") )	kurs_kupna = parser.getText();
						else if	( lastTag.equals("kurs_sprzedazy") )kurs_sprzedazy = parser.getText();
					}

					et = parser.next();
				}
				parser.nextTag(); // przeskakuje z </pozycja> do kolejnego tagu, prawdopodobnie <pozycja>

			} catch (Exception e) {
				Log.w("tlna", e.getMessage());
			}
		}
		
	}
}
