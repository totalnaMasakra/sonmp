package pl.tlna.sonpm.lab1;

import java.text.DecimalFormat;

import android.app.Activity;
import android.os.Bundle;
import android.view.Menu;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends Activity {

	private TextView display;
	private Button sin,cos,tan;
	private static double pam=0,mem=0;
	private static String dispTxt="0";
	private static enum calcState { ADDITION, SUBSTRACT, MULTIPLY, SUBDIVISION, POWER, NOOPP };
	private static calcState cs;
	private static boolean cleanDisplay = false;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		display = (TextView)findViewById(R.id.display);
		sin = (Button)findViewById(R.id.bTrygSin);
		cos = (Button)findViewById(R.id.bTrygCos);
		tan = (Button)findViewById(R.id.bTrygTan);
		cs = calcState.NOOPP;
		writeOnDisplay( dispTxt );
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		//		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	private void writeOnDisplay( double d ){
		dispTxt= (new DecimalFormat("0.############")).format( d ).replace(",", ".");
		if( dispTxt.length() > 12 ) dispTxt= (new DecimalFormat("0.############E0")).format( d ).replace(",", ".");
		display.setText( dispTxt );
		//display.setText( dispTxt= Double.valueOf(d).toString() );

	}
	private void writeOnDisplay( String s ){
		display.setText( dispTxt= s );
	}

	private void updateDisplay(){
		String txt = display.getText().toString();
		switch(cs){
		case ADDITION:
			pam += Double.parseDouble( txt );
			writeOnDisplay(pam);
			break;
		case SUBSTRACT:
			pam -= Double.parseDouble( txt );
			writeOnDisplay(pam);
			break;
		case MULTIPLY:
			pam *= Double.parseDouble( txt );
			writeOnDisplay(pam);
			break;
		case SUBDIVISION:
			pam /= Double.parseDouble( txt );
			writeOnDisplay(pam);
			break;
		case POWER:
			pam = Math.pow( pam , Double.parseDouble( txt ) );
			writeOnDisplay(pam);
			break;
		case NOOPP:
			pam = Double.parseDouble( txt );
			writeOnDisplay(pam);
			break;
		}
		cs = calcState.NOOPP;
		cleanDisplay = true;
	}


	public void bkspClick( View b ){
		String s, txt = display.getText().toString();
		if ( txt.length()>0 )
			if( cleanDisplay == false )
				if( !(s = txt.substring(0, txt.length() - 1) ).equals("") )
					writeOnDisplay( s );
				else
					writeOnDisplay("0");
			else
				writeOnDisplay( "0" );
	}

	public void cClick( View b ) {
		writeOnDisplay( "0" );
		pam=0;
		cs = calcState.NOOPP;
	}

	public void sigClick( View b ){
		String txt = display.getText().toString();
		if (txt.length()>0){
			/*if( txt.substring(0,1).equals("-") ) writeOnDisplay( txt.substring(1) );
			else writeOnDisplay( "-" + txt );*/
			try{ writeOnDisplay( Double.parseDouble(txt) * -1 ); 
			} catch( NumberFormatException e ){
				cClick(null);
				//writeOnDisplay("NumberFormatException");
			}
		}
	}

	public void memClick( View b ){
		String val = ((Button)b).getText().toString() ;
		String txt = display.getText().toString();
		try {
			if (txt.length()>0){
				if( val.equals("MS")) {
					mem = Double.parseDouble( txt );
					cleanDisplay = true;
				}
				else if( val.equals("MA")){
					mem += Double.parseDouble( txt );
					cleanDisplay = true;
				}
				else if( val.equals("MR")) writeOnDisplay(mem);;
			}
		} catch( NumberFormatException e ){
			cClick(null);
			//writeOnDisplay("NumberFormatException");
		}
	}

	public void oppClick( View b ){
		String val = ((Button)b).getText().toString() ;
		String txt = display.getText().toString();

		try{ updateDisplay(); 
		} catch( NumberFormatException e ){
			cClick(null);
			//writeOnDisplay("NumberFormatException");
		}
		if( val.equals("=") ) {
			if (txt.length()>0){
				pam = 0;
			}
		}
		else if( val.equals("+") ) {
			if (txt.length()>0){
				cs = calcState.ADDITION;
			}
		}
		else if( val.equals("-") ) {
			if (txt.length()>0){
				cs = calcState.SUBSTRACT;
			}
		}
		else if( val.equals("*") ) {
			if (txt.length()>0){
				cs = calcState.MULTIPLY;
			}
		}
		else if( val.equals("/") ) {
			if (txt.length()>0){
				cs = calcState.SUBDIVISION;
			}
		}
		else if( val.equals("x^y") ) {
			if (txt.length()>0){
				cs = calcState.POWER;
			}
		}
	}

	public void singOppClick( View b ){
		String val = ((Button)b).getText().toString() ;
		String txt;
		try{
			if( val.equals("tr^-1")){
				Toast.makeText(getApplicationContext(), Double.toString(pam), Toast.LENGTH_SHORT).show();
				if( sin.getText().equals("sin") ){
					sin.setText("asin");
					cos.setText("acos");
					tan.setText("atan");
				}
				else {
					sin.setText("sin");
					cos.setText("cos");
					tan.setText("tan");
				}
			}

			else {
				updateDisplay();
				txt = display.getText().toString();

				if( val.equals("sin") ){
					writeOnDisplay( Math.sin( Double.parseDouble(txt) ) );
					cleanDisplay = true;
				}
				else if( val.equals("cos") ){
					writeOnDisplay( Math.cos( Double.parseDouble(txt) ) );
					cleanDisplay = true;
				}
				else if( val.equals("tan") ){
					writeOnDisplay( Math.tan( Double.parseDouble(txt) ) );
					cleanDisplay = true;
				}
				else if( val.equals("asin") ){
					writeOnDisplay( Math.asin( Double.parseDouble(txt) ) );
					cleanDisplay = true;
				}
				else if( val.equals("acos") ){
					writeOnDisplay( Math.acos( Double.parseDouble(txt) ) );
					cleanDisplay = true;
				}
				else if( val.equals("atan") ){
					writeOnDisplay( Math.atan( Double.parseDouble(txt) ) );
					cleanDisplay = true;
				}
				else if( val.equals("sqrt") ){
					writeOnDisplay( Math.sqrt( Double.parseDouble(txt) ) );
					cleanDisplay = true;
				}
				else if( val.equals("x^2") ){
					writeOnDisplay( Math.pow( Double.parseDouble(txt), 2 ) );
					cleanDisplay = true;
				}
				else if( val.equals("log") ){
					writeOnDisplay( Math.log( Double.parseDouble(txt) ) );
					cleanDisplay = true;
				}
			}
		} catch( NumberFormatException e ){
			cClick(null);
			//writeOnDisplay("NumberFormatException");
		}
	}

	public void numClick( View b ){
		String val = ((Button)b).getText().toString() ;
		String txt = display.getText().toString();

		if( cleanDisplay == true ){
			writeOnDisplay( val );
			cleanDisplay = false;
		}
		else {
			if( val.equals(".") && txt.contains(".") ) {}
			else if( val.equals(".") && txt.length() <= 1 )
				writeOnDisplay( "0." );
			else if ( txt.equals("0") )
				writeOnDisplay( val );
			else
				writeOnDisplay( txt + val );
		}
	}

}
