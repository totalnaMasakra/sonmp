package pl.tlna.sonpm.lab3;

import android.content.Context;
import android.opengl.GLSurfaceView;
import android.view.MotionEvent;

public class ArkanoidGLSurfaceView extends GLSurfaceView {
	
	private MainRenderer renderer;

	public ArkanoidGLSurfaceView(Context c)	{
		super(c);
	}
	
	@Override
	public void setRenderer( Renderer renderer ){
		this.renderer = (MainRenderer)renderer ;
		super.setRenderer( renderer );
	}

	@Override
	public boolean onTouchEvent(MotionEvent e)
	{
		// MotionEvent reports input details from the touch screen
	    // and other input controls. In this case, you are only
	    // interested in events where the touch position changed.

	    float x = e.getX();
	    float y = e.getY();

	    switch (e.getAction()) {

	        case MotionEvent.ACTION_UP:
	        	x = -1;
	        	y = -1;
	        	break;

	    }
	    
	    renderer.setTouch( x, y );

	    return true;
	}
	
}
