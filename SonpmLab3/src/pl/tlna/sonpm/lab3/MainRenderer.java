package pl.tlna.sonpm.lab3;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;
import java.util.Map.Entry;

import javax.microedition.khronos.egl.EGLConfig;
import javax.microedition.khronos.opengles.GL10;

import android.app.Activity;
import android.opengl.GLES20;
import android.opengl.GLSurfaceView;
import android.opengl.Matrix;

/**
 * This class implements our custom renderer. Note that the GL10 parameter
 * passed in is unused for OpenGL ES 2.0 renderers -- the static class GLES20 is
 * used instead.
 */
public class MainRenderer implements GLSurfaceView.Renderer {
	
	/**
	 * Store the model matrix. This matrix is used to move models from object
	 * space (where each model can be thought of being located at the center of
	 * the universe) to world space.
	 */
	private float[] mModelMatrix = new float[16];
	/**
	 * Store the view matrix. This can be thought of as our camera. This matrix
	 * transforms world space to eye space; it positions things relative to our
	 * eye.
	 */
	private float[] mViewMatrix = new float[16];
	/**
	 * Store the projection matrix. This is used to project the scene onto a 2D viewport.
	 */
	private float[] mProjectionMatrix = new float[16];
	/**
	 * Allocate storage for the final combined matrix. This will be passed into
	 * the shader program.
	 */
	private float[] mMVPMatrix = new float[16];

	/** Store our model data in a float buffer. */
	private final FloatBuffer mPaddle, mBall, mBoard, mRLetter,
								mBlockRed, mBlockGreen, mBlockBlue;

	private int mMVPMatrixHandle;	// This will be used to pass in the transformation matrix.
	private int mPositionHandle;	// This will be used to pass in model position information.
	private int mColorHandle;		// This will be used to pass in model color information.
	private final int mBytesPerFloat = 4;	// How many bytes per float.
	private final int mStrideBytes = 7 * mBytesPerFloat;	// How many elements per vertex.
	private final int mPositionOffset = 0;	// Offset of the position data.
	private final int mPositionDataSize = 3;// Size of the position data in elements.
	private final int mColorOffset = 3;		// Offset of the color data.
	private final int mColorDataSize = 4;	// Size of the color data in elements.
	
	
	private int screenWidth, screenHeight;
	private Game game;

	
	private float touchX=-1, touchY=-1;
	
	/**
	 * Initialize the model data.
	 */
	public MainRenderer( Activity parent ) {
		
		game = new Game(parent);
		
		// board
		final float[] board = {
				 2.0f,  1.4f, 0.0f, 0.0f,0.0f,0.0f,1.0f,
				-2.0f,  1.4f, 0.0f, 0.0f,0.0f,0.0f,1.0f,
				 2.0f, -1.5f, 0.0f, 0.0f,0.0f,0.0f,1.0f,
				-2.0f, -1.5f, 0.0f, 0.0f,0.0f,0.0f,1.0f
		};
		mBoard = ByteBuffer.allocateDirect(board.length * mBytesPerFloat).order(ByteOrder.nativeOrder()).asFloatBuffer();
		mBoard.put(board).position(0);
		
		// paddle
		final float[] paddle = {
				 0.3f,  0.1f, 0.0f, 0.4f, 0.4f, 0.3f, 1.0f,
				-0.3f,  0.1f, 0.0f, 0.4f, 0.4f, 0.3f, 1.0f,
				 0.3f, -0.1f, 0.0f, 0.4f, 0.4f, 0.3f, 1.0f,
				-0.3f, -0.1f, 0.0f, 0.4f, 0.4f, 0.3f, 1.0f
		};
		mPaddle = ByteBuffer.allocateDirect(paddle.length * mBytesPerFloat).order(ByteOrder.nativeOrder()).asFloatBuffer();
		mPaddle.put(paddle).position(0);
		
		// ball
		final float[] ball = {
				0.0f,  0.0f,		0.0f, 1.0f, 1.0f, 1.0f, 1.0f,
				0.0f,  0.05f,		0.0f, 1.0f, 1.0f, 1.0f, 1.0f,
				0.0355f, 0.0355f,	0.0f, 1.0f, 1.0f, 1.0f, 1.0f,
				0.05f, 0.0f,		0.0f, 1.0f, 1.0f, 1.0f, 1.0f,
				0.03555f, -0.0355f,	0.0f, 1.0f, 1.0f, 1.0f, 1.0f,
				0.0f, -0.05f,		0.0f, 1.0f, 1.0f, 1.0f, 1.0f,
				-0.03555f, -0.0355f,0.0f, 1.0f, 1.0f, 1.0f, 1.0f,
				-0.05f, 0.0f,		0.0f, 1.0f, 1.0f, 1.0f, 1.0f,
				-0.03555f, 0.0355f,	0.0f, 1.0f, 1.0f, 1.0f, 1.0f,
				0.0f,  0.05f,		0.0f, 1.0f, 1.0f, 1.0f, 1.0f
		};
		mBall = ByteBuffer.allocateDirect(ball.length * mBytesPerFloat).order(ByteOrder.nativeOrder()).asFloatBuffer();
		mBall.put(ball).position(0);
		
		// R letter
		final float[] rLetter = {
				-0.2f,  -0.4f,		0.0f, 1.0f, 1.0f, 1.0f, 1.0f,
				-0.2f,  0.2f,		0.0f, 1.0f, 1.0f, 1.0f, 1.0f,
				0.1f,  0.2f,		0.0f, 1.0f, 1.0f, 1.0f, 1.0f,
				0.2f,  0.1f,		0.0f, 1.0f, 1.0f, 1.0f, 1.0f,
				0.2f,  -0.1f,		0.0f, 1.0f, 1.0f, 1.0f, 1.0f,
				0.1f,  -0.2f,		0.0f, 1.0f, 1.0f, 1.0f, 1.0f,
				-0.2f,  -0.2f,		0.0f, 1.0f, 1.0f, 1.0f, 1.0f,
				0.2f,  -0.4f,		0.0f, 1.0f, 1.0f, 1.0f, 1.0f,
		};
		mRLetter = ByteBuffer.allocateDirect(rLetter.length * mBytesPerFloat).order(ByteOrder.nativeOrder()).asFloatBuffer();
		mRLetter.put(rLetter).position(0);
		
		// blocks
		final float[] blockRed = {
				 0.1f,  0.05f, 0.0f, 1.0f, 0.0f, 0.0f, 1.0f,
				-0.1f,  0.05f, 0.0f, 1.0f, 0.0f, 0.0f, 1.0f,
				 0.1f, -0.05f, 0.0f, 1.0f, 0.0f, 0.0f, 1.0f,
				-0.1f, -0.05f, 0.0f, 1.0f, 0.0f, 0.0f, 1.0f
		};
		mBlockRed = ByteBuffer.allocateDirect(blockRed.length * mBytesPerFloat).order(ByteOrder.nativeOrder()).asFloatBuffer();
		mBlockRed.put(blockRed).position(0);
		final float[] blockGreen = {
				 0.1f,  0.05f, 0.0f, 0.0f, 1.0f, 0.0f, 1.0f,
				-0.1f,  0.05f, 0.0f, 0.0f, 1.0f, 0.0f, 1.0f,
				 0.1f, -0.05f, 0.0f, 0.0f, 1.0f, 0.0f, 1.0f,
				-0.1f, -0.05f, 0.0f, 0.0f, 1.0f, 0.0f, 1.0f
		};
		mBlockGreen = ByteBuffer.allocateDirect(blockGreen.length * mBytesPerFloat).order(ByteOrder.nativeOrder()).asFloatBuffer();
		mBlockGreen.put(blockGreen).position(0);
		final float[] blockBlue = {
				 0.1f,  0.05f, 0.0f, 0.0f, 0.0f, 1.0f, 1.0f,
				-0.1f,  0.05f, 0.0f, 0.0f, 0.0f, 1.0f, 1.0f,
				 0.1f, -0.05f, 0.0f, 0.0f, 0.0f, 1.0f, 1.0f,
				-0.1f, -0.05f, 0.0f, 0.0f, 0.0f, 1.0f, 1.0f
		};
		mBlockBlue = ByteBuffer.allocateDirect(blockBlue.length * mBytesPerFloat).order(ByteOrder.nativeOrder()).asFloatBuffer();
		mBlockBlue.put(blockBlue).position(0);

	}

	public void setTouch( float x, float y ){ touchX = x; touchY = y; }

	@Override
	public void onSurfaceCreated(GL10 glUnused, EGLConfig config) {
		// Set the background clear color to gray.
		GLES20.glClearColor(0.6f, 0.6f, 0.6f, 0.6f);

		// Position the eye behind the origin.
		final float eyeX = 0.0f;
		final float eyeY = 0.0f;
		final float eyeZ = 1.5f;

		// We are looking toward the distance
		final float lookX = 0.0f;
		final float lookY = 0.0f;
		final float lookZ = 1.0f;

		// Set our up vector. This is where our head would be pointing were we holding the camera.
		final float upX = 0.0f;
		final float upY = 1.0f;
		final float upZ = 0.0f;

		// Set the view matrix. This matrix can be said to represent the camera position.
		// NOTE: In OpenGL 1, a ModelView matrix is used, which is a combination of a model and
		// view matrix. In OpenGL 2, we can keep track of these matrices separately if we choose.
		Matrix.setLookAtM(mViewMatrix, 0, eyeX, eyeY, eyeZ, lookX, lookY, lookZ, upX, upY, upZ);

		final String vertexShader = "uniform mat4 u_MVPMatrix; \n" // A constant representing the combined model/view/projection matrix.
				+ "attribute vec4 a_Position; \n" // Per-vertex position information we will pass in.
				+ "attribute vec4 a_Color; \n" // Per-vertex color information we will pass in.
				+ "varying vec4 v_Color; \n" // This will be passed into the fragment shader.
				+ "void main() \n" // The entry point for our vertex shader.
				+ "{ \n" + " v_Color = a_Color; \n" // Pass the color through to the fragment shader. It will be interpolated across the triangle.
				+ " gl_Position = u_MVPMatrix \n" // gl_Position is a special variable used to store the final position.
				+ " * a_Position; \n" // Multiply the vertex by the matrix to get the final point in
				+ "} \n"; // normalized screen coordinates.

		final String fragmentShader = "precision mediump float; \n" // Set the default precision to medium. We don't need as high of a precision in the fragment shader.
				+ "varying vec4 v_Color; \n" // This is the color from the vertex shader interpolated across the triangle per fragment.
				+ "void main() \n" // The entry point for our fragment shader.
				+ "{ \n" + " gl_FragColor = v_Color; \n" // Pass the color directly through the pipeline.
				+ "} \n";

		// Load in the vertex shader.
		int vertexShaderHandle = GLES20.glCreateShader(GLES20.GL_VERTEX_SHADER);	
		if (vertexShaderHandle != 0) {
			GLES20.glShaderSource(vertexShaderHandle, vertexShader);	// Pass in the shader source.
			GLES20.glCompileShader(vertexShaderHandle);					// Compile the shader.
			final int[] compileStatus = new int[1];						// Get the compilation status.
			GLES20.glGetShaderiv(vertexShaderHandle, GLES20.GL_COMPILE_STATUS, compileStatus, 0);
			if (compileStatus[0] == 0) {								// If the compilation failed, delete the shader.
				GLES20.glDeleteShader(vertexShaderHandle);
				vertexShaderHandle = 0;
			}
		}
		if (vertexShaderHandle == 0) throw new RuntimeException("Error creating vertex shader.");

		// Load in the fragment shader shader.
		int fragmentShaderHandle = GLES20.glCreateShader(GLES20.GL_FRAGMENT_SHADER);
		if (fragmentShaderHandle != 0) {
			GLES20.glShaderSource(fragmentShaderHandle, fragmentShader);	// Pass in the shader source.
			GLES20.glCompileShader(fragmentShaderHandle);					// Compile the shader.
			final int[] compileStatus = new int[1];							// Get the compilation status.
			GLES20.glGetShaderiv(fragmentShaderHandle, GLES20.GL_COMPILE_STATUS, compileStatus, 0);
			if (compileStatus[0] == 0) {									// If the compilation failed, delete the shader.
				GLES20.glDeleteShader(fragmentShaderHandle);
				fragmentShaderHandle = 0;
			}
		}
		if (fragmentShaderHandle == 0) throw new RuntimeException("Error creating fragment shader.");
		
		// Create a program object and store the handle to it.
		int programHandle = GLES20.glCreateProgram();
		if (programHandle != 0) {
			GLES20.glAttachShader(programHandle, vertexShaderHandle);	// Bind the vertex shader to the program.
			GLES20.glAttachShader(programHandle, fragmentShaderHandle);	// Bind the fragment shader to the program.
			GLES20.glBindAttribLocation(programHandle, 0, "a_Position");// Bind attributes
			GLES20.glBindAttribLocation(programHandle, 1, "a_Color");
			GLES20.glLinkProgram(programHandle);						// Link the two shaders together into a program.
			final int[] linkStatus = new int[1];						// Get the link status.
			GLES20.glGetProgramiv(programHandle, GLES20.GL_LINK_STATUS, linkStatus, 0);
			if (linkStatus[0] == 0) {									// If the link failed, delete the program.
				GLES20.glDeleteProgram(programHandle);
				programHandle = 0;
			}
		}
		if (programHandle == 0)	throw new RuntimeException("Error creating program.");

		// Set program handles. These will later be used to pass in values to the program.
		mMVPMatrixHandle = GLES20.glGetUniformLocation(programHandle, "u_MVPMatrix");
		mPositionHandle = GLES20.glGetAttribLocation(programHandle, "a_Position");
		mColorHandle = GLES20.glGetAttribLocation(programHandle, "a_Color");

		// Tell OpenGL to use this program when rendering.
		GLES20.glUseProgram(programHandle);
		
	}
	
	@Override
	public void onSurfaceChanged(GL10 glUnused, int width, int height) {
		// Set the OpenGL viewport to the same size as the surface.
		GLES20.glViewport(0, 0, width, height);

		screenWidth = width;
		screenHeight = height;

		final float ratio = (float) width / height;
		final float left = -ratio;
		final float right = ratio;
		final float bottom = -1.0f;
		final float top = 1.0f;
		final float near = 1.0f;
		final float far = 3.0f;

		Matrix.frustumM(mProjectionMatrix, 0, left, right, bottom, top, near, far);
	}
	
	@Override
	public void onDrawFrame(GL10 glUnused) {
		long startTime = System.currentTimeMillis();
		
		GLES20.glClear(GLES20.GL_DEPTH_BUFFER_BIT | GLES20.GL_COLOR_BUFFER_BIT);
		
		game.nextFrame( touchX/screenWidth , touchY/screenHeight );

		Matrix.setIdentityM(mModelMatrix, 0);
		Matrix.translateM(mModelMatrix, 0, 0 , 0, 0.0f);
		preparePolygon(mBoard);
		GLES20.glDrawArrays(GLES20.GL_TRIANGLE_STRIP, 0, 4);
		
		Matrix.setIdentityM(mModelMatrix, 0);
		Matrix.translateM(mModelMatrix, 0, -2.24f, 1.28f, 0.0f);
		Matrix.scaleM(mModelMatrix, 0, 0.7f, 0.7f, 1.0f);
		preparePolygon(mRLetter);
		GLES20.glDrawArrays(GLES20.GL_LINE_STRIP, 0, 8);
		
		Matrix.setIdentityM(mModelMatrix, 0);
		Matrix.translateM(mModelMatrix, 0, game.paddleX , game.paddleY, 0.0f);
		preparePolygon(mPaddle);
		GLES20.glDrawArrays(GLES20.GL_TRIANGLE_STRIP, 0, 4);
		
		/* blocks loop */
		for ( Entry<Game.Pair, Integer> e : game.blockList.entrySet() ) {
			Matrix.setIdentityM(mModelMatrix, 0);
			Matrix.translateM(mModelMatrix, 0, e.getKey().x * 0.2f , e.getKey().y * 0.1f , 0.0f);
			switch( e.getValue() ){
			case Game.BLOCK_RED:
				preparePolygon(mBlockRed);
				break;
			case Game.BLOCK_GREEN:
				preparePolygon(mBlockGreen);
				break;
			case Game.BLOCK_BLUE:
				preparePolygon(mBlockBlue);
				break;
			}
			GLES20.glDrawArrays(GLES20.GL_TRIANGLE_STRIP, 0, 4);
		}

		Matrix.setIdentityM(mModelMatrix, 0);
		Matrix.translateM(mModelMatrix, 0, game.ballX, game.ballY, 0.0f);
		preparePolygon(mBall);
		GLES20.glDrawArrays(GLES20.GL_TRIANGLE_FAN, 0, 10);
		
		try {
			long slp = 20 - ( System.currentTimeMillis() - startTime );
//			Log.i("fps", "Milis used to render frame: " + Long.toString( 20 - slp ));
			if( slp>0 ) Thread.sleep( slp );
		} catch (InterruptedException e) {}
	}

	/**
	 * Draws a triangle from the given vertex data.
	 * 
	 * @param aTriangleBuffer
	 *            The buffer containing the vertex data.
	 */
	private void preparePolygon(final FloatBuffer aTriangleBuffer) {
		// Pass in the position information
		aTriangleBuffer.position(mPositionOffset);
		GLES20.glVertexAttribPointer(mPositionHandle, mPositionDataSize, GLES20.GL_FLOAT, false, mStrideBytes, aTriangleBuffer);

		GLES20.glEnableVertexAttribArray(mPositionHandle);

		// Pass in the color information
		aTriangleBuffer.position(mColorOffset);
		GLES20.glVertexAttribPointer(mColorHandle, mColorDataSize, GLES20.GL_FLOAT, false, mStrideBytes, aTriangleBuffer);

		GLES20.glEnableVertexAttribArray(mColorHandle);

		// This multiplies the view matrix by the model matrix, and stores the
		// result in the MVP matrix
		// (which currently contains model * view).
		Matrix.multiplyMM(mMVPMatrix, 0, mViewMatrix, 0, mModelMatrix, 0);

		// This multiplies the modelview matrix by the projection matrix, and
		// stores the result in the MVP matrix
		// (which now contains model * view * projection).
		Matrix.multiplyMM(mMVPMatrix, 0, mProjectionMatrix, 0, mMVPMatrix, 0);

		GLES20.glUniformMatrix4fv(mMVPMatrixHandle, 1, false, mMVPMatrix, 0);
	}
}