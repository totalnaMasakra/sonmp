package pl.tlna.sonpm.lab3;

import java.util.HashMap;

import android.app.Activity;

public class Game {

	private Activity parent;
	
	public float ballX, ballY, ballOldX, ballOldY, accelX, accelY, ballSpeed;
	public float paddleX, paddleY, paddleMove;
	public int points, level;
	
	public static final int BLOCK_RED = 1;
	public static final int BLOCK_GREEN = 2;
	public static final int BLOCK_BLUE = 3;
	
	HashMap<Pair, Integer> blockList;
	
	public Game( Activity parent ) {
		this.parent = parent;
		reset();
		accelX = accelY = paddleMove = 0;
		ballY = 99.9f;
	}
	
	public void reset(){
		ballSpeed=0.03f; paddleY=-1.3f;
		paddleMove=0.08f;
		points=0; level=0;
		blockList = new HashMap<Pair, Integer>();
		loadLevel();
	}
	
	/* 
	 * levels
	 * x - definiuje przesunięcie od środka w lewo/prawo
	 * y - od środka w górę
	 */
	private void loadLevel(){
		ballX=0.0f; ballY=-1.2f;
		accelX=1; accelY=1;
		paddleX=0.0f;
		try {
			switch (level++) {
			case 0:
				blockList.put(new Pair(0, 0), BLOCK_RED);
				blockList.put(new Pair(2, 0), BLOCK_RED);
				blockList.put(new Pair(-2, 0), BLOCK_RED);
				blockList.put(new Pair(1, 2), BLOCK_GREEN);
				blockList.put(new Pair(2, 2), BLOCK_GREEN);
				blockList.put(new Pair(-1, 2), BLOCK_GREEN);
				blockList.put(new Pair(-2, 2), BLOCK_GREEN);
				blockList.put(new Pair(1, 3), BLOCK_BLUE);
				blockList.put(new Pair(-1, 3), BLOCK_BLUE);
				blockList.put(new Pair(3, 3), BLOCK_BLUE);
				blockList.put(new Pair(-3, 3), BLOCK_BLUE);
				blockList.put(new Pair(5, 3), BLOCK_BLUE);
				blockList.put(new Pair(-5, 3), BLOCK_BLUE);
				break;
			default:
				level = 0;
				blockList.put(new Pair(1,2), BLOCK_RED);
			}
		} catch ( PairBadValuesException e ){}
	}
	
	public void nextFrame( float touchX, float touchY ){

		//Log.d("tlna", "x: " + Float.toString(touchX) + ", y: " + Float.toString(touchY) );
		if( touchX < 0.1f && touchY < 0.2f && touchX >= 0 && touchY >= 0 ) reset();

		final float rot;
		
		// przesuwanie paletki
		if( touchX >= 0 ){
			rot = touchX * 2.0f - 1.0f ;
			if		( rot >= 0 && paddleX < 1.61f ) paddleX += paddleMove;
			else if	( rot <  0 && paddleX > -1.61f ) paddleX -= paddleMove;
		}
		
		// game over
		if( ballY < -1.403f ) {
			accelX = accelY = paddleMove = 0;
			ballY = 99.9f;
//			reset();
			
			((MainActivity)parent).showToast("Zdobyłes punktów: " + Integer.toString(points));
			
		}
		
		// ball motion
		ballOldX = ballX;
		ballX += ballSpeed * accelX ;
		ballOldY = ballY;
		ballY += ballSpeed * accelY ;

		// border collision
		if( ballX > 1.95f ) accelX = -1 * Math.abs(ballX)  ;
		if( ballX < -1.95f ) accelX = Math.abs(ballX)  ;
		if( ballY > 1.35f ) accelY *= -1 ;
		// paddle collision
		if( ballY-0.05f < paddleY+0.1f && ballX > paddleX-0.3f && ballX < paddleX+0.3f ) {
			accelY = (0.3f - Math.abs( ballX - paddleX ) ) * 6.0f ;
			accelX = (ballX - paddleX) * 7.0f ;
		}
		// block collision
		int checkX = Math.round( ballX * 5 );
		int checkY = Math.round( ballY * 10);
//		Log.d("tlna", Integer.toString(checkX) + ", " + Integer.toString(checkY) );
		try{
			// rzuci wyjątek, jeśli nie ma kolizji
			// jeśli jest, wykona kolejne instrukcje
			Integer ret = blockList.remove( new Pair( checkX , checkY ) );
			if (ret != null) {
				points += 1;
				float blockX = checkX / 5.0f;
//				float blockY = checkY / 10.0f;
				float v = accelX*accelX + accelY*accelY ;
//				if( ballY < blockY + 0.05f && ballY > blockY - 0.05f ) accelX *= -1;
//				if( ballX < blockX + 0.1f && ballX > blockX - 0.1f ) accelY *= -1;
				accelX = (ballX - blockX) * 12.0f;
				accelY = (float) Math.sqrt( v - accelX*accelX ) * Math.signum(accelY) * -0.5f;
			}
			if( blockList.isEmpty() ) loadLevel();
		}catch(Exception e){}
	}

	
	
	public class Pair{
		public final float x, y;
		public Pair( float x, float y) throws PairBadValuesException {
			this.x = x;
			if( Math.abs(x) > 9 ) throw new PairBadValuesException("x is illegal");
			this.y = y;
			if( y>18 || y < 0 ) throw new PairBadValuesException("y is illegal");
		}
		@Override
		public boolean equals(Object o) {
			Pair another;
			if( o instanceof Pair ) another = (Pair)o;
			else return false;
			if( this.x==another.x && this.y==another.y ) return true;
			else return false;
		}
		@Override
		public int hashCode(){
			return Math.round(x*y*100);
		}
	}
	public class PairBadValuesException extends Exception {
		public PairBadValuesException(String msg){ super(msg); }
		private static final long serialVersionUID = -6521793563365518908L;}
}
